import unittest
import requests
import json
import os

IP = "0.0.0.0"
PORT = os.environ.get('TEST_PORT')
URL = "https://" + IP + ":" + PORT

class MainDeployTest(unittest.TestCase):

    def test_home(self):
        response = requests.get(URL + "/", verify=False)
        self.assertEqual(200, response.status_code)

if __name__ == "__main__":
    unittest.main()


