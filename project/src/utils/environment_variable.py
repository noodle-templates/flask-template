import os

def get(variable_name):
	if variable_name in os.environ:
		return os.environ[variable_name]
	else:
		raise Exception("Environment variable named ("+ variable_name +") does not exist and it should")