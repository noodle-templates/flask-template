from flask import Flask, request, jsonify
from utils import environment_variable
from configuration.config import DevelopmentConfig, BaseConfig, ProductionConfig

ENV = environment_variable.get("FLASK_ENV")
PORT = environment_variable.get("PORT")
app = Flask(__name__)

#####################################################################
if ENV == 'production':
    app.config.from_object(BaseConfig)
    ssl_context = ProductionConfig.get_ssl_context()
else:
    ssl_context = DevelopmentConfig.get_ssl_context()
    app.config.from_object(DevelopmentConfig)
#####################################################################

@app.route('/', methods=['GET'])
def home():
    return jsonify({"msg":"success"}), 200

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=PORT, ssl_context=ssl_context)