from utils import environment_variable
import os
import ssl

class BaseConfig:
    DEBUG = False
    TESTING = False

class DevelopmentConfig(BaseConfig):
    DEBUG = True

    @staticmethod
    def get_ssl_context():
        # Specify the paths to your self-signed SSL certificate and private key
        ssl_cert_path = os.path.join(os.path.dirname(os.path.abspath(__file__)), '../certs/self_signed.crt')
        ssl_key_path = os.path.join(os.path.dirname(os.path.abspath(__file__)), '../certs/self_signed.key')

        # Create and return the SSL context
        ssl_context = ssl.SSLContext(ssl.PROTOCOL_TLS_SERVER)
        ssl_context.load_cert_chain(ssl_cert_path, keyfile=ssl_key_path)
        return ssl_context

class ProductionConfig(BaseConfig):
    pass

    @staticmethod
    def get_ssl_context():
        # Specify the paths to your SSL certificate and private key
        ssl_cert_path = environment_variable.get("SSL_PATH_CRT")
        ssl_key_path = environment_variable.get("SSL_PATH_KEY")
        
        # Create and return the SSL context
        ssl_context = ssl.SSLContext(ssl.PROTOCOL_TLS_SERVER)
        ssl_context.load_cert_chain(ssl_cert_path, keyfile=ssl_key_path)
        return ssl_context
