#!/bin/bash -e

txtgrn='\e[0;32m' #Green
txtwht='\e[0;37m' #White
txtred='\e[0;31m' # Red

echo "Deploying test container..."

./scripts/deploy.sh

echo -e "${txtwht}Paused for 5 seconds${txtreset}"
sleep 5

echo "Deployment tests..."
TEST_PORT=8443 python3 -m unittest discover -s project/ -p dtest_*.py -v

# If there are errors, exit with a non-zero status
if [ $? -ne 0 ]; then
  echo -e "${txtred}Tests failed, aborting commit.${txtreset}"
  exit 1
else
  # Print success message in green
  echo -e "${txtgrn}Tests completed successfully.${txtreset}"
fi

