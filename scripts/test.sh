#!/bin/bash

echo "Launching tests..."

./project/scripts/run_deployment_tests.sh

if [ $? -ne 0 ]; then
  echo "Deployment Tests failed"
  exit 1
fi
