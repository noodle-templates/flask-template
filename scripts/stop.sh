#!/bin/bash

# Check if containers are already running
if [ "$(docker ps -q -f name=cnt_mycloud_api)" ]; then
    echo -e "${txtred}Stopping containers...${txtwht}"

    # Stop and remove containers
    docker-compose -f docker/docker-compose.yml --project-directory . down

    # Check the exit status of the docker-compose command
    if [ $? -eq 0 ]; then
        echo -e "${txtred}Containers stopped.${txtwht}"
    else
        echo -e "${txtred}Error: Failed to stop containers.${txtwht}"
        exit 1
    fi
else
    echo -e "${txtwht}No running containers found.${txtwht}"
fi

echo -e "${txtred}Deleting images...${txtwht}"

# Remove images
docker image rm img_mycloud_api

# Check the exit status of the docker image rm command
if [ $? -eq 0 ]; then
    echo -e "${txtred}Images deleted.${txtwht}"
else
    echo -e "${txtred}Error: Failed to delete images.${txtwht}"
    exit 1
fi
