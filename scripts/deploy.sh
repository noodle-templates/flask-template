#!/bin/bash

txtgrn='\e[0;32m' #Green
txtwht='\e[0;37m' #White
txtred='\e[0;31m' # Red

# Check if docker-compose.yml exists
if [ ! -f config/.env ]; then
    echo -e "${txtred}Error: .env file not found.${txtwht}"
    exit 1
fi

# Check if docker-compose.yml exists
if [ ! -f docker/docker-compose.yml ]; then
    echo -e "${txtred}Error: docker-compose.yml not found.${txtwht}"
    exit 1
fi

./scripts/stop.sh

echo -e "${txtgrn} Launching containers... ${txtwht}"

docker-compose -f docker/docker-compose.yml --project-directory . --env-file config/.env up -d

# Check the exit status of the docker-compose command
if [ $? -eq 0 ]; then
    echo -e "${txtgrn} Containers deployed successfully.${txtred}"
else
    echo -e "${txtred}Error: Failed to deploy containers.${txtgrn}"
fi